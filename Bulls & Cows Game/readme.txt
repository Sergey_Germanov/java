author: Sergey Germanov

I added some ASCII art (http://ascii.co.uk/) to make the interface of the game more appealing to play.

I used draw.io (https://www.draw.io/) to draw diagrams.

