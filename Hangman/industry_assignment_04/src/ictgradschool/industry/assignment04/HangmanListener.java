package ictgradschool.industry.assignment04;

/**
  * author: Sergey Germanov (sger197)
 */
public interface HangmanListener {
    void update(Game game);
}
