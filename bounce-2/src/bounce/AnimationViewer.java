package bounce;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;


/**
 * Simple GUI program to show an animation of shapes. Class AnimationViewer is
 * a special kind of GUI component (JPanel), and as such an instance of
 * AnimationViewer can be added to a JFrame object. A JFrame object is a
 * window that can be closed, minimised, and maximised. The state of an
 * AnimationViewer object comprises a list of Shapes and a Timer object. An
 * AnimationViewer instance subscribes to events that are published by a Timer.
 * In response to receiving an event from the Timer, the AnimationViewer iterates
 * through a list of Shapes requesting that each Shape paints and moves itself.
 *
 * @author Ian Warren
 */
@SuppressWarnings("serial")
public class AnimationViewer extends JPanel implements ActionListener {
    // Frequency in milliseconds for the Timer to generate events.
    private static final int DELAY = 20;

    // Collection of Shapes to animate.
    private List<Shape> _shapes;

    private Timer _timer = new Timer( DELAY,this );

    /**
     * Creates an AnimationViewer instance with a list of Shape objects and
     * starts the animation.
     */
    public AnimationViewer() {
        _shapes = new ArrayList<Shape>();

        // Populate the list of Shapes.
        RectangleShape rect = new RectangleShape( 0,0,2,3,90,50 );
        NestingShape parent = new NestingShape( 0,0,2,3,200,300 );
        DynamicRectangleShape dynamic = new DynamicRectangleShape( 10,10,1,3,40,40 );
        OvalShape oval = new OvalShape( 50,10,5,7,85,85 );
        //Add images
        ImageShape img1 = new ImageShape( 15,3,6,6,64,64,"Butters-icon.png" );
        ImageShape img2 = new ImageShape( 35,5,8,3,64,64,"Cartman-icon.png" );
        ImageShape img3 = new ImageShape( 25,8,1,2,64,64,"Kenny-icon.png" );
        ImageShape img4 = new ImageShape( 55,1,2,5,64,64,"Kyle-icon.png" );


        parent.add( rect );
        parent.add( dynamic );
        parent.add( img1 );
        parent.add( img4 );

        _shapes.add( parent );
        _shapes.add( img2 );
        _shapes.add( img3 );
        _shapes.add( oval );
        // Add some _text to some shapes

        dynamic.setTextColor( Color.orange );
        dynamic.setText( "PARK" );
        rect.setTextColor( Color.blue );
        rect.setText( "SOUTH" );

        oval.setText( "This is the Sun" );


        // Start the animation.
        _timer.start();
    }

    /**
     * Called by the Swing framework whenever this AnimationViewer object
     * should be repainted. This can happen, for example, after an explicit
     * repaint() call or after the window that contains this AnimationViewer
     * object has been opened, exposed or moved.
     */
    public void paintComponent( Graphics g ) {
        // Call inherited implementation to handle background painting.
        super.paintComponent( g );

        // Calculate bounds of animation screen area.
        int width = getSize().width;
        int height = getSize().height;

        // Create a GraphicsPainter that Shape objects will use for drawing.
        // The GraphicsPainter delegates painting to a basic Graphics object.
        Painter painter = new GraphicsPainter( g );

        // Progress the animation.
        for (Shape s : _shapes) {
            s.paint( painter );
            s.move( width,height );
        }
    }

    /**
     * Notifies this AnimationViewer object of an ActionEvent. ActionEvents are
     * received by the Timer.
     */
    public void actionPerformed( ActionEvent e ) {
        // Request that the AnimationViewer repaints itself. The call to
        // repaint() will cause the AnimationViewer's paintComponent() method
        // to be called.
        repaint();
    }


    /**
     * Main program method to create an AnimationViewer object and display this
     * within a JFrame window.
     */
    public static void main( String[] args ) {
        javax.swing.SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                JFrame frame = new JFrame( "Animation viewer" );
                frame.add( new AnimationViewer() );

                // Set window properties.
                frame.setSize( 500,500 );
                frame.setLocationRelativeTo( null );
                frame.setVisible( true );
            }
        } );
    }
}
