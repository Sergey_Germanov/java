package bounce;

import java.awt.*;

public class OvalShape extends Shape {

    public OvalShape() {
        super();
    }

    public OvalShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public OvalShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }



    @Override
    protected void doPaint( Painter painter ) {
        painter.setColor( Color.orange );
        painter.fillOval(_x,_y,_width,_height);
    }
}
