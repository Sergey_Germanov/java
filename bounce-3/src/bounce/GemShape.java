package bounce;

public class GemShape extends Shape {

    public GemShape(int x, int y) {
        super( x,y );
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }
    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height, String text) {
        super(x,y,deltaX,deltaY,width,height,text);
    }

    @Override
    public void doPaint(Painter painter) {
        int pointOneY = _y+_height/2;
        int pointTwoX = _x+_width/2;
        int pointTwoXtwo = _x+20;
        int pointThreeX = _x+_width;
        int pointThreeXtwo = _x+(_width-20);
        int pointThreeY = _y+_height/2;
        int pointFourX = pointTwoX;
        int pointFourY = _y+_height;





        if (_width >= 40) {
            painter.drawLine(_x, pointOneY, pointTwoXtwo, _y);
            painter.drawLine(pointTwoXtwo, _y, pointThreeXtwo, _y);
            painter.drawLine(pointThreeXtwo, _y, pointThreeX, pointThreeY);
            painter.drawLine(pointThreeX, pointThreeY, pointThreeXtwo, pointFourY);
            painter.drawLine(pointThreeXtwo, pointFourY, pointTwoXtwo, pointFourY);
            painter.drawLine(pointTwoXtwo, pointFourY, _x, pointOneY);
        }else {
            painter.drawLine(_x, pointOneY, pointTwoX, _y);
            painter.drawLine(pointTwoX, _y, pointThreeX, pointThreeY);
            painter.drawLine(pointThreeX, pointThreeY, pointFourX, pointFourY);
            painter.drawLine(pointFourX, pointFourY, _x, pointOneY);
        }

    }


}
