package bounce;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {
    BufferedImage picture;
    public ImageShape(int x, int y) {
        super( x,y );
    }

    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String imageURL) {
        super(x,y,deltaX,deltaY,width,height);
        try {
            picture = ImageIO.read(new File(imageURL));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint( Painter painter ) {

        painter.drawImage(picture, _x, _y,_width, _height);

    }

    @Override
    protected void doPaint( Painter painter ) {

    }
}
