package bounce;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Task1 implements TreeModel {

    protected ShapeModel _adaptee;


    public Task1( ShapeModel model ) {
        _adaptee = model;
    }


    @Override
    public Object getRoot() {
        return _adaptee.root();
    }

    @Override
    public Object getChild( Object parent,int index ) {
        Object result = null;

        if (parent instanceof NestingShape) {

            NestingShape nestShape = (NestingShape) parent;

            if (nestShape.getNumberOfChildren() > index) {
                result = nestShape.getShape( index );
            }
        }
        return result;
    }

    @Override
    public int getChildCount( Object parent ) {
        int result = 0;

        Shape shape = (Shape) parent;

        if (shape instanceof NestingShape) {
            NestingShape nestShape = (NestingShape) shape;
            result = nestShape.getNumberOfChildren();
        }
        return result;
    }

    @Override
    public boolean isLeaf( Object node ) {

        if (node instanceof NestingShape) {
            return false;
        }

        return true;

    }

    @Override
    public void valueForPathChanged( TreePath path,Object newValue ) {

    }

    @Override
    public int getIndexOfChild( Object parent,Object child ) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape nestShape = (NestingShape) parent;
            List<Shape> shapesList = nestShape.getList();
            Iterator<Shape> i = shapesList.iterator();
            boolean found = false;

            int index = 0;
            while (!found && i.hasNext()) {
                Shape current = i.next();
                if (child == current) {
                    found = true;
                    indexOfChild = index;
                } else {
                    index++;
                }
            }
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener( TreeModelListener l ) {

    }

    @Override
    public void removeTreeModelListener( TreeModelListener l ) {

    }
}
