package bounce;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;


import java.util.ArrayList;
import java.util.List;

public class Task2 extends Task1 implements ShapeModelListener {

    private List<TreeModelListener> list = new ArrayList<TreeModelListener>();


    public Task2( ShapeModel model ) {
        super( model );
    }


    @Override
    public void addTreeModelListener( TreeModelListener e ) {
        list.add( e );
    }

    @Override
    public void removeTreeModelListener( TreeModelListener e ) {
        list.remove( e );
    }

    @Override
    public void update( ShapeModelEvent event ) {

        ShapeModelEvent.EventType _type = event._type;
        int index;
        Shape operand;
        NestingShape parent = event._parent;

        TreeModelEvent obj;



        switch (_type) {
            case ShapeAdded:
                operand = event._operand;
                index = event._index;
                obj = new TreeModelEvent( this,parent.path().toArray(),new int[]{index},new Object[]{operand} );
                for (TreeModelListener i : list) {
                    i.treeNodesInserted( obj );
                }
                break;
            case ShapeRemoved:
                operand = event._operand;
                index = event._index;
                obj = new TreeModelEvent( this,parent.path().toArray(),new int[]{index},new Object[]{operand} );
                for (TreeModelListener i : list) {
                    i.treeNodesRemoved( obj );
                }
                break;
        }
    }
}
