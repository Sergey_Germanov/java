package bounce.forms;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


import javax.imageio.ImageIO;
import javax.swing.*;

import bounce.ImageRectangleShape;
import bounce.NestingShape;
import bounce.ShapeModel;
import bounce.forms.util.Form;
import bounce.forms.util.FormHandler;


public class ImageShapeFormHandler implements FormHandler {

    private ShapeModel _model;
    private NestingShape _parentOfNewShape;
    File imageFile;
    long startTime;
    int width;
    int deltaX;
    int deltaY;
    BufferedImage scaledImage;

    public ImageShapeFormHandler(
            ShapeModel model,
            NestingShape parent) {
        _model = model;
        _parentOfNewShape = parent;
    }

    @Override
    public void processForm(Form form) {
        startTime = System.currentTimeMillis();

        // Read field values from the form.
        imageFile = (File) form.getFieldValue(File.class, ImageFormElement.IMAGE);
        width = form.getFieldValue(Integer.class, ShapeFormElement.WIDTH);
        deltaX = form.getFieldValue(Integer.class, ShapeFormElement.DELTA_X);
        deltaY = form.getFieldValue(Integer.class, ShapeFormElement.DELTA_Y);


        MySwingWorker worker = new MySwingWorker();
        worker.execute();


    }

    private class MySwingWorker extends SwingWorker<BufferedImage, Void> {

        @Override
        protected BufferedImage doInBackground() throws Exception {
            // Load the original image (ImageIO.read() is a blocking call).
            BufferedImage fullImage = null;
            try {
                fullImage = ImageIO.read(imageFile);
            } catch (IOException e) {
                System.out.println("Error loading image.");
            }

            int fullImageWidth = fullImage.getWidth();
            int fullImageHeight = fullImage.getHeight();

            scaledImage = fullImage;

            // Scale the image if necessary.
            if (fullImageWidth > width) {
                double scaleFactor = (double) width / (double) fullImageWidth;
                int height = (int) ((double) fullImageHeight * scaleFactor);

                scaledImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                Graphics2D g = scaledImage.createGraphics();


                g.drawImage(fullImage, 0, 0, width, height, null);


            }
            return scaledImage;
        }

        @Override
        public void done() {
            // Create the new Shape and add it to the model.
            ImageRectangleShape imageShape = new ImageRectangleShape(deltaX, deltaY, scaledImage);
            _model.add(imageShape, _parentOfNewShape);


            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println("Image loading ans scaling took " + elapsedTime + "ms.");
        }

    }
}


