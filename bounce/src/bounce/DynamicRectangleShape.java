package bounce;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class DynamicRectangleShape extends Shape {

    Color color;
    boolean boundariesX = false;
    boolean boundariesY = false;

    public DynamicRectangleShape(Color color) {
        super();
        this.color = color;
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void move(int width, int height) {
        int deltaX = _deltaX;
        int deltaY = _deltaY;

        super.move(width, height);
        if (deltaX != _deltaX) {
            boundariesX = true;
            boundariesY = false;
        }

        if (deltaY != _deltaY) {
            boundariesY = true;
            boundariesX = false;
        }



    }

    @Override
    public void paint(Painter painter) {



        if (boundariesX) {
            Random r = new Random();
            painter.setColor(new Color(r.nextInt(256), r.nextInt(256), r.nextInt(256)));

        }


        if (boundariesY) {
            painter.setColor(Color.black);
            painter.drawRect(_x, _y, _width, _height);

        } else {
            painter.fillRect(_x, _y, _width, _height);
        }

    }
}
