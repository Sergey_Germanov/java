package bounce;

import java.awt.*;
import java.awt.image.ImageObserver;

/**
 * Implementation of the Painter interface that delegates drawing to a
 * java.awt.Graphics object.
 *
 * @author Ian Warren
 */
public class GraphicsPainter implements Painter {
    // Delegate object.
    private Graphics _g;
    private Color color;

    /**
     * Creates a GraphicsPainter object and sets its Graphics delegate.
     */
    public GraphicsPainter(Graphics g) {
        this._g = g;
    }

    /**
     * see bounce.Painter.drawRect
     */
    public void drawRect(int x, int y, int width, int height) {
        _g.drawRect(x, y, width, height);
    }

    /**
     * see bounce.Painter.drawOval
     */
    public void drawOval(int x, int y, int width, int height) {
        _g.drawOval(x, y, width, height);
    }

    /**
     * see bounce.Painter.drawLine.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        _g.drawLine(x1, y1, x2, y2);
    }

    @Override
    public void fillRect(int x, int y, int width, int height) {
            _g.fillRect(x, y, width, height);
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
        _g.setColor(color);
    }

    @Override
    public void drawImage( Image img,int x,int y, int width, int height, ImageObserver observer ) {
        _g.drawImage(img, x, y, width, height, null );
    }
}
