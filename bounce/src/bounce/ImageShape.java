package bounce;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.awt.Graphics;

public class ImageShape extends Shape {
    BufferedImage img;
    public ImageShape() {
        super();
    }

    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String imageURL) {
        super(x,y,deltaX,deltaY,width,height);
        try {
            img = ImageIO.read(new File(imageURL));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint( Painter painter ) {

        painter.drawImage(img, _x, _y,_width, _height,null);

    }
}
