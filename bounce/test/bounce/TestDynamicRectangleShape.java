package bounce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDynamicRectangleShape {

    private MockPainter _painter;

    @Before
    public void setUp() {
        _painter = new MockPainter();
    }

    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffTop() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 10, 12, -15);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        assertEquals("(filled rectangular 10,10,25,35,null)(rectangle 22,0,25,35)"
                + "(rectangle 34,15,25,35)", _painter.toString());
    }


    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffBottom() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 90, 12, 15);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(filled rectangular 10,90,25,35,null)(rectangle 22,100,25,35)"
                + "(rectangle 34,85,25,35)", _painter.toString());
    }


    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffRight() {
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 20, 12, 15);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        String colorA = _painter.getColor().toString();
        shape.move(135, 10000);
        shape.paint(_painter);

        String colorB = _painter.getColor().toString();
        assertEquals("(filled rectangular 100,20,25,35,null)(filled rectangular 110,35,25,35," + colorA + ")"
                + "(filled rectangular 98,50,25,35," + colorB + ")", _painter.toString());
    }


    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffLeft() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 20, -12, 15);
        shape.paint(_painter);
        shape.move(10000, 10000);
        shape.paint(_painter);
        String colorA = _painter.getColor().toString();
        shape.move(10000, 10000);
        shape.paint(_painter);
        String colorB = _painter.getColor().toString();
        assertEquals("(filled rectangular 10,20,25,35,null)(filled rectangular 0,35,25,35," + colorA + ")"
                + "(filled rectangular 12,50,25,35," + colorB + ")", _painter.toString());
    }

    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffBottomAndRight() {
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 90, 12, 15);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(filled rectangular 100,90,25,35,null)(rectangle 100,100,25,35)"
                + "(rectangle 88,85,25,35)", _painter.toString());
    }


    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffBottomAndLeft() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 90, -12, 15);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(filled rectangular 10,90,25,35,null)(rectangle 0,100,25,35)"
                + "(rectangle 12,85,25,35)", _painter.toString());
    }

    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffTopAndRight() {
        DynamicRectangleShape shape = new DynamicRectangleShape(100, 10, 12, -15);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(filled rectangular 100,10,25,35,null)(rectangle 100,0,25,35)"
                + "(rectangle 88,15,25,35)", _painter.toString());
    }

    @Test
    public void testDynamicRectangleShapeMoveWithBounceOffTopAndLeft() {
        DynamicRectangleShape shape = new DynamicRectangleShape(10, 10, -12, -15);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(filled rectangular 10,10,25,35,null)(rectangle 0,0,25,35)"
                + "(rectangle 12,15,25,35)", _painter.toString());
    }

}
