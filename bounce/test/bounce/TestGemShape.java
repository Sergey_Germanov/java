package bounce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGemShape {
    private MockPainter _painter;


    @Before
    public void setUp() {
        _painter = new MockPainter();
    }


    @Test
    public void testSimpleMoveSmall() {
        GemShape shape = new GemShape(100, 20, 12, 15, 20, 20);
        shape.paint(_painter);
        shape.move(500, 500);
        shape.paint(_painter);
        assertEquals("(line 100,30,110,20)(line 110,20,120,30)(line 120,30,110,40)(line 110,40,100,30)(line 112,45,122,35)(line 122,35,132,45)(line 132,45,122,55)(line 122,55,112,45)",
                _painter.toString());
    }


    @Test
    public void testSimpleMoveRegular() {
        GemShape shape = new GemShape(100, 20, 12, 15, 45, 45);
        shape.paint(_painter);
        shape.move(500, 500);
        shape.paint(_painter);
        assertEquals("(line 100,42,120,20)(line 120,20,125,20)(line 125,20,145,42)(line 145,42,125,65)(line 125,65,120,65)(line 120,65,100,42)(line 112,57,132,35)(line 132,35,137,35)(line 137,35,157,57)(line 157,57,137,80)(line 137,80,132,80)(line 132,80,112,57)",
                _painter.toString());
    }


    @Test
    public void testShapeMoveWithBounceOffRightSmall() {
        GemShape shape = new GemShape(100, 20, 12, 15, 20, 20);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        assertEquals("(line 100,30,110,20)(line 110,20,120,30)(line 120,30,110,40)(line 110,40,100,30)(line 112,45,122,35)(line 122,35,132,45)(line 132,45,122,55)(line 122,55,112,45)"
                + "(line 115,60,125,50)(line 125,50,135,60)(line 135,60,125,70)(line 125,70,115,60)", _painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffRightRegular() {
        GemShape shape = new GemShape(100, 20, 12, 15, 45, 45);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        shape.move(135, 10000);
        shape.paint(_painter);
        assertEquals("(line 100,42,120,20)(line 120,20,125,20)(line 125,20,145,42)(line 145,42,125,65)(line 125,65,120,65)(line 120,65,100,42)(line 90,57,110,35)(line 110,35,115,35)(line 115,35,135,57)(line 135,57,115,80)(line 115,80,110,80)(line 110,80,90,57)"
                + "(line 78,72,98,50)(line 98,50,103,50)(line 103,50,123,72)(line 123,72,103,95)(line 103,95,98,95)(line 98,95,78,72)", _painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffLeftSmall() {
        GemShape shape = new GemShape(10, 20, -12, 15, 20, 20);
        shape.paint(_painter);
        shape.move(10000, 10000);
        shape.paint(_painter);
        shape.move(10000, 10000);
        shape.paint(_painter);
        assertEquals("(line 10,30,20,20)(line 20,20,30,30)(line 30,30,20,40)(line 20,40,10,30)(line 0,45,10,35)(line 10,35,20,45)(line 20,45,10,55)(line 10,55,0,45)"
                + "(line 12,60,22,50)(line 22,50,32,60)(line 32,60,22,70)(line 22,70,12,60)", _painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffLeftRegular() {
        GemShape shape = new GemShape(10, 20, -12, 15, 45, 45);
        shape.paint(_painter);
        shape.move(10000, 10000);
        shape.paint(_painter);
        shape.move(10000, 10000);
        shape.paint(_painter);
        assertEquals("(line 10,42,30,20)(line 30,20,35,20)(line 35,20,55,42)(line 55,42,35,65)(line 35,65,30,65)(line 30,65,10,42)(line 0,57,20,35)(line 20,35,25,35)(line 25,35,45,57)(line 45,57,25,80)(line 25,80,20,80)(line 20,80,0,57)"
                + "(line 12,72,32,50)(line 32,50,37,50)(line 37,50,57,72)(line 57,72,37,95)(line 37,95,32,95)(line 32,95,12,72)", _painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffBottomAndRightSmall() {
        GemShape shape = new GemShape(10, 90, -12, 15, 20, 20);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(line 10,100,20,90)(line 20,90,30,100)(line 30,100,20,110)(line 20,110,10,100)(line 0,115,10,105)(line 10,105,20,115)(line 20,115,10,125)(line 10,125,0,115)"
                + "(line 12,125,22,115)(line 22,115,32,125)(line 32,125,22,135)(line 22,135,12,125)", _painter.toString());
    }

    @Test
    public void testShapeMoveWithBounceOffBottomAndRightRegular() {
        GemShape shape = new GemShape(10, 90, -12, 15, 45, 45);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        shape.move(125, 135);
        shape.paint(_painter);
        assertEquals("(line 10,112,30,90)(line 30,90,35,90)(line 35,90,55,112)(line 55,112,35,135)(line 35,135,30,135)(line 30,135,10,112)(line 0,112,20,90)(line 20,90,25,90)(line 25,90,45,112)(line 45,112,25,135)(line 25,135,20,135)(line 20,135,0,112)"
                + "(line 12,97,32,75)(line 32,75,37,75)(line 37,75,57,97)(line 57,97,37,120)(line 37,120,32,120)(line 32,120,12,97)", _painter.toString());
    }
}
