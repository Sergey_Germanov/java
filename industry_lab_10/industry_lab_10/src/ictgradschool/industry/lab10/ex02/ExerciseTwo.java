package ictgradschool.industry.lab10.ex02;

/**
 * Created by anhyd on 27/03/2017.
 */
public class ExerciseTwo {

    /**
     * Returns the sum of all positive integers between 1 and num (inclusive).
     */
    public int getSum( int num ) {

        if (num == 1) {
            return 1;
        }
        return num + (getSum( num - 1 ));

    }


    /**
     * Returns the smallest value in the given array, between the given first (inclusive) and second (exclusive) indices
     *
     * @param nums        the array
     * @param firstIndex  the inclusive lower index
     * @param secondIndex the exclusive upper index
     */
    public int getSmallest( int[] nums,int firstIndex,int secondIndex ) {
        if (secondIndex - firstIndex == 1) {
            return nums[firstIndex];
        } else if (secondIndex - firstIndex == 2) {
            return Math.min( nums[firstIndex],nums[firstIndex + 1] );
        } else {
            return Math.min( nums[firstIndex],getSmallest( nums,firstIndex + 1,secondIndex ) );
        }
    }

    /**
     * Prints all ints from n down to 1.
     */
    public void printNums1( int n ) {
        System.out.println( n );
        if (n > 1) {
            printNums1( n - 1 );
        }

    }

    /**
     * Prints all ints from 1 up to n.
     */
    public void printNums2( int n ) {

        if (n > 1) {
            printNums2( n - 1 );
        }
        System.out.println( n );
    }

    /**
     * Returns the number of 'e' and 'E' characters in the given String.
     *
     * @param input the string to check
     */
    public int countEs( String input ) {

        if (input == null || input.length() == 0) {

            return 0;
        } else if (input.length() == 1) {
            return (input.toLowerCase().charAt( 0 ) == 'e') ? 1 : 0;
        } else {
            int num = (input.toLowerCase().charAt( 0 ) == 'e') ? 1 : 0;
            return num + countEs( input.substring( 1 ) );
        }
    }

    /**
     * Returns the nth number in the fibonacci sequence.
     */
    public int fibonacci( int n ) {

        if (n == 0) return 0;
        else if (n == 1) return 1;
        else return fibonacci( n - 1 ) + fibonacci( n - 2 );

    }

    /**
     * Returns true if the given input String is a palindrome, false otherwise.
     *
     * @param input the String to check
     */
    public boolean isPalindrome( String input ) {

          if (input == null || input.length() < 2) {
            return true;
        } else if (input.length() == 2) {
            return input.charAt( 0 ) == input.charAt( 1 );
        } else {
            return input.charAt( 0 ) == input.charAt( input.length() - 1 ) && isPalindrome( input.substring( 1,input.length() - 1 ) );
        }

    }

}
